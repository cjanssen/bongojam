function love.load()
	setVideoMode(true)
	love.graphics.setBlendMode("additive")

	initSounds()

	gamestate = 0
	initData()

	bongoLimit = 24
end

function setVideoMode(fullscreen)
	isFullscreen = fullscreen
	local flags = {isFullscreen, true, 0, false, false, true}

	if not isFullscreen then
		love.graphics.setMode(1024,768,isFullscreen)
		scaleH = 1
		scaleV = 1
		ofsH = 0
		ofsV = 0
	else
		local list = love.graphics.getModes()

		local W = list[1].width
		local H = list[1].height
		scaleH = W/1024
		scaleV = H/768

		ofsH = 0
		ofsV = 0
		if scaleV < scaleH then
			scaleH = scaleV
			ofsH = (W - 1024)/4
		elseif scaleH < scaleV then
			scaleV = scaleH
			ofsV = (H - 768)/4
		end

		love.graphics.setMode(W,H,isFullscreen)
	end
end

function initData()
	unfocused = {}
	flareimg = love.graphics.newImage("img/bmj.png")
	flarewh = flareimg:getWidth()/2
	flarehh = flareimg:getHeight()/2

	friction = 5
	energize = 40

	ww = 7 --13
	hh = 5 --9
	cellsize = 140
	celloffs = cellsize/4
		
	for x=1,ww do
		unfocused[x] = {}
		for y = 1,hh do
			unfocused[x][y] = getCirclePack(
				x, -- x
				y, -- y
				50, -- r
				7 -- subcircles
			)
		end
	end

	titlePack = getCirclePack(1,2,40,17)
	titlePack.text = "BongoJam"
	titlePack.font = love.graphics.newFont(88)
	subtitlePack = getCirclePack(1,3,30,17)
	subtitlePack.font = love.graphics.newFont(44)
	subtitlePack.text = "by Christiaan Janssen and Kirill Krysov"
	
	if subtitlePack.circle.pile == titlePack.circle.pile then
		subtitlePack.circle.pile = (subtitlePack.circle.pile+1) % 6
		subtitlePack.circle.col = colorForState(subtitlePack.circle.pile + 1)
	end
end

function initSounds()
	MouseInSnd = {
		love.audio.newSource("snd/ogg/clikC.ogg","stream"),
		love.audio.newSource("snd/ogg/clikD.ogg","stream"),
		love.audio.newSource("snd/ogg/clikE.ogg","stream"),
		love.audio.newSource("snd/ogg/clikG.ogg","stream"),
		love.audio.newSource("snd/ogg/clikA.ogg","stream"),
		love.audio.newSource("snd/ogg/clikC2.ogg","stream")
	}
end

function colorForState(state)
	if state <= 1 then return {238, 240, 38} end
	if state == 2 then return {255, 34, 18} end
	if state == 3 then return {10, 194, 185} end
	if state == 4 then return { 230, 92, 0} end
	if state == 5 then return {15, 209, 60} end
	if state >= 6 then return {21, 29, 238} end

end

function getCirclePack(_x, _y, _r, _n)
	local _pile = math.random(6) - 1
	local circlePack = { circle = {
		ix = _x,
		iy = _y,
		x = _x * cellsize - celloffs,
		y = _y * cellsize - celloffs,
		r = _r,
		w = _r/2,
		pile = _pile,
		col = colorForState(_pile+1),
		fadeTimer = 0,
		ticked = false,
		entered = false
	} }

	for i=1,_n do
		circlePack[i] = {
			dir = math.random() * math.pi * 2,
			speed = math.random() * 0.5 + 3,
			t = math.random() * math.pi * 2,
			arg = math.random() * 0.15 + 0.1,
			rotspd = math.random() - 0.5,
			dx = 0,
			dy = 0
		}
	end

	return circlePack
end

function drawCirclePack(cp)
	love.graphics.setColor(cp.circle.col[1], cp.circle.col[2], cp.circle.col[3], 64)
	love.graphics.setLineWidth(cp.circle.w)

	if cp.text then
		love.graphics.setFont(cp.font)
	end

	for i,v in ipairs(cp) do
		-- love.graphics.circle("line", cp.circle.x + v.dx, cp.circle.y + v.dy, cp.circle.r, cp.circle.r)
		if cp.text then
			love.graphics.print(cp.text, cp.circle.x + v.dx, cp.circle.y + v.dy)
		else
			love.graphics.draw(flareimg, cp.circle.x + v.dx, cp.circle.y + v.dy,0,0.5,0.5,flarewh, flarehh)
		end
	end
end

function updateCirclePack(dt, cp)
	-- global
	if cp.circle.fadeTimer > 0 then
		local destCol = colorForState(cp.circle.pile + 1)
		for i,v in ipairs(destCol) do
			cp.circle.col[i] = (1-cp.circle.fadeTimer)*v + cp.circle.fadeTimer * 255
		end

		cp.circle.fadeTimer = cp.circle.fadeTimer - dt
		if cp.circle.fadeTimer < 0.5 and not cp.circle.ticked then
			circleDone(cp)
		end
		if cp.circle.fadeTimer <= 0 then
			cp.circle.ticked = false
			cp.circle.col = colorForState(cp.circle.pile + 1)
		end
	end



	-- individual circles
	local dx = (love.mouse.getX()-ofsH)/scaleH - cp.circle.x
	local dy = (love.mouse.getY()-ofsV)/scaleV - cp.circle.y
	local distSq = math.pow(20 + dx*dx + dy*dy,0.35)
	local amp = math.min((1 + cp.circle.r/distSq), 4)

	for i,v in ipairs(cp) do
		v.t = v.t + dt * v.speed * amp
		v.dir = v.dir + dt * v.rotspd * amp
		v.dx = math.cos(v.dir) * math.sin(v.t) * v.arg * cp.circle.r * amp
		v.dy = math.sin(v.dir) * math.sin(v.t) * v.arg * cp.circle.r * amp
	end

	-- mouse brush sounds
	local mouseDist = math.sqrt(dx*dx+dy*dy)
	if cp.circle.entered and mouseDist > cp.circle.r then
		cp.circle.entered = false
	elseif not cp.circle.entered and mouseDist < cp.circle.r then
		cp.circle.entered = true
		if gamestate == 1 then
			soundit(cp)
		end
	end
end

function soundit(cp)
	local ndx = cp.circle.pile + 1
	if ndx < 1 then ndx = 1 end
	if ndx > table.getn(MouseInSnd) then ndx = table.getn(MouseInSnd) end
	local snd = MouseInSnd[ndx]
	snd:stop()
	snd:play()
end

function startFade(cp)
	cp.circle.pile = cp.circle.pile + 1
	-- extra energy
	if math.random(energize) == 1 then
		cp.circle.pile = cp.circle.pile + 1
	end
	cp.circle.fadeTimer = 1
	cp.circle.ticked = false

	local ix = cp.circle.ix
	local iy = cp.circle.iy
	-- extra sound
	playBongo((cp.circle.x - 512)/512)
end

function playBongo(panning)
	if love.audio.getNumSources() > bongoLimit then
		return
	end

	bongosound = love.audio.newSource("snd/bongo.ogg","stream")
	bongosound:setPitch(1 + math.random()*0.5 - 0.25)
	bongosound:setPosition(panning,0,0)
	bongosound:play()

end

function circleDone(cp)
	cp.circle.ticked = true
	if cp.circle.pile > 4 then
		cp.circle.pile = cp.circle.pile - 4

		-- friction (energy escape)
		if math.random(friction) == 1 then
			cp.circle.pile = cp.circle.pile - 1
		end
		local ix = cp.circle.ix
		local iy = cp.circle.iy

		startFade(unfocused[ix%ww+1][iy])
		startFade(unfocused[(ix+ww-2)%ww+1][iy])
		startFade(unfocused[ix][(iy+hh-2)%hh+1])
		startFade(unfocused[ix][iy%hh+1])
	end

end

function love.draw()
	love.graphics.push()
	love.graphics.translate(ofsH, ofsV)
	love.graphics.scale(scaleH, scaleV)
	if gamestate == 0 then
		drawCirclePack(titlePack)
		drawCirclePack(subtitlePack)
	elseif gamestate == 1 then
		for i,v in ipairs(unfocused) do
			for j,w in ipairs(v) do
				drawCirclePack(w)
			end
		end
	end
	love.graphics.pop()
end

function love.update(dt)
	if gamestate == 0 then
		updateCirclePack(dt, titlePack)
		updateCirclePack(dt, subtitlePack)
	elseif gamestate == 1 then
		for i,v in ipairs(unfocused) do
			for j,w in ipairs(v) do 
				updateCirclePack(dt, w)
			end
		end
	end
end

function love.mousepressed(x,y)
	if gamestate == 0 then
		playBongo(0)
		gamestate = 1
	elseif gamestate == 1 then
		local sx = (x-ofsH)/scaleH
		local sy = (y-ofsV)/scaleV
		local ix = math.floor((sx-cellsize/2+celloffs) / cellsize) + 1
		local iy = math.floor((sy-cellsize/2+celloffs) / cellsize) + 1

		if ix <= 0 or ix > table.getn(unfocused) then return end
		if iy <= 0 or iy > table.getn(unfocused[ix]) then return end

		if unfocused[ix][iy].circle.fadeTimer <= 0 then
			startFade(unfocused[ix][iy])
		end
	end
end

function love.keypressed(key)
	if key=="escape" then
		love.event.push("quit")
	end

	if key=="f1" then
		setVideoMode(not isFullscreen)
	end
end