Bongojam
--------
by Christiaan Janssen and Kirill Krysov
Berlin Mini Game Jam December 2013


Objective
---------
There is no objective, this is an interactive toy.


Controls
--------
Mouse: interaction
ESC: exit
F1: toggle fullscreen
